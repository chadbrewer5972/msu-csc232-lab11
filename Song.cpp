/**
 * @file Song.cpp
 * @brief Implementation of Song class nethods.
 */

#include "Song.h"

Song::Song(const std::string& artist, const std::string& title) :
		_artist(artist), _title(title) {
}

std::string Song::getArtist() const {
	return _artist;
}

std::string Song::getTitle() const {
	return _title;
}

Song::~Song() {
}
