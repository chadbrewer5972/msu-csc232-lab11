/**
 * @file Song.h
 * @brief Song class specification.
 */

#ifndef SONG_H_
#define SONG_H_

#include <string>

class Song {
private:
	std::string _artist;
	std::string _title;
public:
	Song(const std::string& artist, const std::string& title);
	std::string getArtist() const;
	std::string getTitle() const;
	virtual ~Song();
};

#endif /* SONG_H_ */
